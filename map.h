#ifndef MAP_H
#define MAP_H


struct tile
{
    unsigned char symbol;
    bool slimed;
    bool isWalkable;
};

class map
{
private:
    tile _map[GAMEAREAX][GAMEAREAY];
public:
    map();
    virtual ~map();
    void createMap(void);
    unsigned char getTileSymbol(unsigned x, unsigned y);
    bool getIsSlimed(unsigned x, unsigned y);
    bool getIsWalkable(unsigned x, unsigned y);
    void gameOfLife(void);
    unsigned getWallCount(unsigned x, unsigned y);
    void setTileState(unsigned x, unsigned y, bool slimed, bool walkable);
};

#endif // MAP_H
