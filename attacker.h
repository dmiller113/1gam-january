#ifndef ATTACKER_H
#define ATTACKER_H


class attacker
{
protected:
private:
    int _basePower;
    int _power;
public:
    attacker();
    attacker(int power);
    virtual ~attacker();
    int getPower(void) const;
    void calcPower(void);
    int attack(actor* owner, actor* target);  //return value is the damage
};

#endif // ATTACKER_H
