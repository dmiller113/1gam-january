#ifndef MAIN_H
#define MAIN_H
//just include everything all at once.
#include <iostream>
#include "libtcod.hpp"
#include "defines.h"
#include "messageQueue.h"
#include "destructable.h"
#include "attacker.h"
#include "effects.h"
#include "pickable.h"
#include "ai.h"
#include "actor.h"
#include "map.h"
#include "engine.h"

#endif
