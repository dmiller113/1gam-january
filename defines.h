#ifndef DEFINES_H
#define DEFINES_H

#include "main.h"
const int ARENARC = 7;
const unsigned GAMEAREAX = 49;
const unsigned GAMEAREAY = 49;
const unsigned MESSAGEQUEUEY = 5;
const int BORDER = 3;
const int XPADDING = 20;
const int UI = 4;
const unsigned WINDOWX = GAMEAREAX + BORDER + XPADDING;
const unsigned WINDOWY = GAMEAREAY + UI + MESSAGEQUEUEY + BORDER;
const unsigned GAMEBOARDT1 = 10;
const unsigned GAMEBOARDT2 = GAMEBOARDT1 + GAMEAREAX + 1;

enum GAMESTATES
{
    gsSetup,
    gsPlayerAct,
    gsPlayerMoved,
    gsPlayerActed,
    gsAnimation,
    gsIdle,
    gsMenu,
    gsOption,
    gsTopScore,
    gsDead,
    gsGameOver
};

enum DIRECTIONS
{
    dSELF,
    dNORTH,
    dWEST,
    dSOUTH,
    dEAST,
    dERROR
};

enum MONSTERTYPE
{
    mANT,
    mBULLY,
    mDIGGER,
    mPULSERD,
    mPULSERO,
    mSHOOTER,
    mTACKLE,
    mWURM,
    mMIDBOSS,
};
extern GAMESTATES gs;
#endif
