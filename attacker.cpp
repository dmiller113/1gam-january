#include "main.h"

attacker::attacker()
{
    //ctor
    _basePower = 1;
    _power = 1;
}

attacker::attacker(int power)   :
_basePower(power), _power(power)
{
    ;
}

attacker::~attacker()
{
    //dtor
}

int attacker::getPower(void) const
{
    return _power;
}

void attacker::calcPower(void)
{
    _power = _basePower;
}

int attacker::attack(actor* owner, actor* target)  //return value is the damage
{
    gameEngine.msgQueue.addMessage(owner->name + " is attacking!");
    target->destruct->attacked(target, owner, _power);
    return 0;
}
