#include "main.h"

void programSetup(void);
void menuSetup(void);
void gameSetup(void);

GAMESTATES menuInput(const TCOD_key_t& t);

GAMESTATES gs  = gsMenu;
engine gameEngine;

int main(void)
{
    bool programContinue = true;
    //setup the program
    programSetup();
    while(!TCODConsole::isWindowClosed() and programContinue)
    {
        //setup the menu
        menuSetup();
        while(!TCODConsole::isWindowClosed() && gs == gsMenu)
        {
            //handle menu
            TCOD_key_t key = TCODConsole::checkForKeypress();
            gs = menuInput(key);
            if(gs == gsGameOver)
                programContinue = false;
        }
        //setup game loop
        gameSetup();
        gs = gsPlayerAct;
        while(!TCODConsole::isWindowClosed() && programContinue && gs != gsGameOver)
        {
            //gameloop
            gameEngine.render();
            TCODConsole::flush();
            gameEngine.gameTick();
        }
        gameEngine.cleanup();
        TCODConsole::root->clear();
        if(gs == gsGameOver)
            gs = gsMenu;
    }
}


void programSetup(void)
{
    TCODConsole::initRoot(WINDOWX, WINDOWY, "January Game");
    TCODSystem::setFps(60);
    TCODConsole::setColorControl(TCOD_COLCTRL_1, TCODColor::red, TCODColor::black);
    TCODConsole::setColorControl(TCOD_COLCTRL_2, TCODColor::blue, TCODColor::black);
    TCODConsole::setColorControl(TCOD_COLCTRL_3, TCODColor::green, TCODColor::black);
}


void menuSetup(void)
{
    //Draws the parts of the menu that won't change from user input and sets up any variables that the menu needs.
    TCODConsole::root->print(5,1, "Try and survive!");
    TCODConsole::root->print(5, 3, "You are trapped inside a sinister, living arena with only a");
    TCODConsole::root->print(5, 4, "belt shield, an energy sword, and a bow. Can you outlive");
    TCODConsole::root->print(5, 5, "your enemies and escape?");
    TCODConsole::root->print(5, 7, "Move with the numpad or hjkyuibnm");
    TCODConsole::root->print(5, 9, "Press f to fire an arrow up to 6 tiles away in any");
    TCODConsole::root->print(5, 10, "orthagonal direction. You can fire once every 5 turns.");
    TCODConsole::root->print(5, 12, "Move onto * to upgrade your abilities");
    TCODConsole::root->print(5, 13, "%c*%c upgrade your melee, allowing it to echo behind the enemy hit.", TCOD_COLCTRL_1, TCOD_COLCTRL_STOP);
    TCODConsole::root->print(5, 14, "%c*%c upgrade your ranged attack, allowing you to fire more often.", TCOD_COLCTRL_3, TCOD_COLCTRL_STOP);
    TCODConsole::root->print(5, 15, "%c*%c upgrade your shield, causing it to recharge faster.", TCOD_COLCTRL_2, TCOD_COLCTRL_STOP);
    TCODConsole::root->print(5, 17, "Move onto ? to pickup an item. Then press Space to use it.");
    TCODConsole::root->print(5, 18, "Each item does something different, and you can only hold one.");
    TCODConsole::root->print(5, 19, "Try and figure out what each does!");
    TCODConsole::root->print(5, 21, "Your energy shield can only take one hit before shorting out");
    TCODConsole::root->print(5, 22, "and needing recharging. Take care, you won't survive an attack");
    TCODConsole::root->print(5, 23, "without it!");
    TCODConsole::root->print(15, 40, "a) New Game");
    TCODConsole::root->print(15, 42, "b) Top Scores");
    TCODConsole::root->print(15, 44, "c) Options");
    TCODConsole::root->print(15, 46, "q) Quit");
    TCODConsole::flush();
}

GAMESTATES menuInput(const TCOD_key_t& t)
{
    switch(t.c)
    {
        case 'a':
        {
            //New Game
            return gsIdle;
            break;
        }
        case 'b':
        {
            //Top Scores
            return gsTopScore;
            break;
        }
        case 'c':
        {
            //Option
            return gsOption;
            break;
        }
        case 'q':
        {
            //Quit
            return gsGameOver;
            break;
        }
        default:
        {
            return gsMenu;
            break;
        }

    }
}

void gameSetup(void)
{
    TCODConsole::root->clear();
    for(unsigned x = 0; x < WINDOWX; x++)
    {
        for(unsigned y = 0; y < WINDOWY; y++)
        {
            if(x == 0 || x == WINDOWX - 1 || ((x == GAMEBOARDT1 || x == GAMEBOARDT2) && y < GAMEAREAY + 1) )
                TCODConsole::root->putCharEx(x, y, TCOD_CHAR_DVLINE, TCODColor::white, TCODColor::black);
            if(y == 0 || y == WINDOWY - 1 || (y == GAMEAREAY + 1 && x > GAMEBOARDT1 && x < GAMEBOARDT2 ) )
                TCODConsole::root->putCharEx(x, y, TCOD_CHAR_DHLINE, TCODColor::white, TCODColor::black);
            if(x == 0 && y == 0)
                TCODConsole::root->putCharEx(x, y, TCOD_CHAR_DNW, TCODColor::white, TCODColor::black);
            if( (x == 0 && y == WINDOWY - 1) || (x == GAMEBOARDT1 && y == GAMEAREAY+1))
                TCODConsole::root->putCharEx(x, y, TCOD_CHAR_DSW, TCODColor::white, TCODColor::black);
            if(x == WINDOWX - 1 && y == 0)
                TCODConsole::root->putCharEx(x, y, TCOD_CHAR_DNE, TCODColor::white, TCODColor::black);
            if( (x == WINDOWX - 1 && y == WINDOWY - 1) || (x == GAMEBOARDT2 && y == GAMEAREAY+1))
                TCODConsole::root->putCharEx(x, y, TCOD_CHAR_DSE, TCODColor::white, TCODColor::black);
            if( (x == GAMEBOARDT1 && y == 0) || (x == GAMEBOARDT2 && y == 0) )
                TCODConsole::root->putCharEx(x, y, TCOD_CHAR_DTEES, TCODColor::white, TCODColor::black);

        }
    }
    gameEngine.init();
}
