#ifndef DESTRUCTABLE_H
#define DESTRUCTABLE_H

class actor;
class ai;

class destructable
{
protected:
    int _shieldAmount;
    int _maxShield;
    int _hp;
    int _maxHP;
public:
    int _shieldTick;
    int _shieldRecharge;
    int _regenTick;
    int _regen;
public:
    destructable();
    destructable(int shieldAmount, int shieldRecharge, int HP, int regen);
    virtual ~destructable();
    virtual void update(void);
    bool damage(actor* owner, int amount);
    bool heal(actor* owner, int amount, bool overHealOK = false);
    unsigned attacked(actor* attackee, actor* attacker, unsigned amount);
    bool isDead(void) const;
    virtual void die(actor* owner);


};

class playerDestructable    :   public destructable
{
public:
    playerDestructable(int shieldAmount, int shieldRecharge, int HP, int regen);
    void update(void);
    void die(actor* owner);
};

class monsterDestructable   :   public destructable
{
private:
    int _pointValue;
public:
    ai* corpseAI;
public:
    monsterDestructable(int shieldAmount, int shieldRecharge, int HP, int regen, int pointsWorth, ai* corpse);
    ~monsterDestructable(void);
    void update(void);
    void die(actor* owner);
};
#endif // DESTRUCTABLE_H
