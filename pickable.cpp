#include "main.h"

pickable::pickable()
{
    //ctor
    //make a default pickable
    _charges = 1;
    //target select for what actors get effected by the item's effect
    _targetSelector = NULL;
    //effect of the item
    _effect = NULL;

}

pickable::pickable(unsigned charges, targetSelection* TS, effect* eff)    :
    _charges(charges), _targetSelector(TS), _effect(eff)
{
    ;
}

pickable::~pickable()
{
    //dtor
}

bool pickable::pick(actor* owner, actor* wearer)
{
    if(_charges == 0)
    {
        //this is an upgrade, immediately effect the player with it.
        ;//apply the effect
        gameEngine.msgQueue.addMessage("Applied an upgrade on " + wearer->name);
    }else
    {
        //this is a consumable item. Put it into the player's inventory if they have nothing held,
        //otherwise offer to swap their item with the new one.
        ;
    }
    return true;
}

container::container(int cSize)  :
    size(cSize)
{
    //ctor
    ;
}

container::~container()
{
    inventory.clearAndDelete();
}

bool container::add(actor* object)
{
    if( size == 0 || ( (size > 0) && (inventory.size() >= size) ) )
    {
        //dat inventory is too full foo
        return false;
    }
    inventory.push(object);
    return true;
}

void container::remove(actor* object)
{
    inventory.remove(object);
}

actor* container::getItem(int pos)
{
    if(pos >= inventory.size())
        return NULL;
    return inventory.get(pos);
}

bool pickable::use(actor* owner, actor* user)
{
    TCODList<actor*> objectList;
    DIRECTIONS dChoice = getDirection();
    bool succeed = false;
    if(dChoice == dERROR)
        return succeed;
    if(_targetSelector)
    {
        //isn't a self use, find out the direction you're using it in.
        ;//for testing purpose fixed direction
        _targetSelector->selectTargets(user, objectList, dChoice);

    }else
    {
        objectList.push(user);
    }

    for(actor** it = objectList.begin(); it != objectList.end(); it++)
    {
        if(_effect->applyTo(*it))
            succeed = true;
    }

    if(succeed && owner != NULL)
    {
        user->itemsHeld->remove(owner);
        delete owner;
    }
    return succeed;
}


DIRECTIONS pickable::getDirection(void)
{
    TCODConsole* console = new TCODConsole(49, 1);
    console->print(0,0,"Pick a direction (24586, nhjuk, other to cancel):");

    TCODConsole::blit(console, 0, 0, 0, 0, TCODConsole::root, GAMEBOARDT1+1, 1);
    TCODConsole::flush();
    TCODConsole::waitForKeypress(true);
    TCOD_key_t key = TCODConsole::waitForKeypress(true);
    switch(key.c)
    {
        case '2':
        case 'n':
            return dSOUTH;
            break;
        case '4':
        case 'h':
            return dWEST;
            break;
        case '8':
        case 'u':
            return dNORTH;
            break;
        case '6':
        case 'k':
            return dEAST;
            break;
        case '5':
        case 'j':
            return dSELF;
            break;
        default:
            return dERROR;

    }
}
