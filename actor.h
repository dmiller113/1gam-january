#ifndef ACTOR_H
#define ACTOR_H

#include <string>

class actor
{
private:
    unsigned _x;
    unsigned _y;     //The actor's x,y coords on the map.
protected:
public:
    std::string name;
    int symbol; //Actor's ascii symbol.
    TCODColor foreColor;
    TCODColor backColor;
    bool isBlocking;
    std::string description;
    attacker* atker;
    destructable* destruct;
    ai* actorAI;
    pickable* item;
    container* itemsHeld;

    //functions
    actor();
    actor(unsigned x = 0, unsigned y = 0, std::string nm = "undefined", std::string desc = "unknown", int ch = '@', TCODColor fColor = TCODColor::white, TCODColor bColor = TCODColor::black, bool blocks = true);
    virtual ~actor();
    void move(unsigned newX, unsigned newY);
    unsigned getX(void) const {return _x;}
    unsigned getY(void) const {return _y;}
    void render(TCODConsole* drawable) const;
    void update(void);
    std::string returnDescription(void);
    float getDistance(unsigned x2, unsigned y2);
};

#endif // ACTOR_H
