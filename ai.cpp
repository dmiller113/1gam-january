#include "main.h"
#include <iostream>
#include <math.h>

//Defines


//Globals


//Enums


//Classes


ai::ai()
{
    //ctor
}

ai::~ai()
{
    //dtor
}

bool ai::moveOrAttack(actor* owner, unsigned newX, unsigned newY)
{
    //check for slime
    //damage if you move onto slime
    //display a message so they know what's happening.
    if(gameEngine._curMap.getIsSlimed(newX, newY))
    {
        if(owner == gameEngine._player )
            gameEngine.msgQueue.addMessage("Ouch, the slime burns!", TCODColor::green);
    }
    return true;
}

void playerAI::update(actor* owner)
{
    TCOD_key_t playerInput;
    TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &playerInput, NULL);
    unsigned tempX = owner->getX();
    unsigned tempY = owner->getY();

    if(playerInput.c == 'Q')
        gs = gsGameOver;

    if(gs == gsPlayerAct)
    {
        switch (playerInput.c)
        {
            case 'h':
            case '4':
                tempX -= 1;
                gs = gsPlayerMoved;
                break;
            case 'k':
            case '6':
                tempX += 1;
                gs = gsPlayerMoved;
                break;
            case '2':
            case 'n':
                tempY += 1;
                gs = gsPlayerMoved;
                break;
            case 'u':
            case '8':
                tempY -= 1;
                gs = gsPlayerMoved;
                break;
            case 'y':
            case '7':
                tempX -= 1;
                tempY -= 1;
                gs = gsPlayerMoved;
                break;
            case 'i':
            case '9':
                tempX += 1;
                tempY -= 1;
                gs = gsPlayerMoved;
                break;
            case 'b':
            case '1':
                tempX -= 1;
                tempY += 1;
                gs = gsPlayerMoved;
                break;
            case 'm':
            case '3':
                tempX += 1;
                tempY += 1;
                gs = gsPlayerMoved;
                break;
            case '5':
                gs = gsPlayerActed;
                break;
            case 'f':
                //fire an arrow
                ;
                //find the object effected
                if(_arrow && gameEngine._arrowCharge == gameEngine._maxCharge)
                {
                    if(!_arrow->use(NULL, gameEngine._player) )
                        gameEngine.msgQueue.addMessage("Missed...", TCODColor::gold);
                    gs = gsPlayerActed;
                    gameEngine._arrowCharge = 0;
                }else
                {
                    gameEngine.msgQueue.addMessage("You can't fire yet.", TCODColor::gold);
                }
                //I acted so advance time
                break;
            case ' ':
                std::cout<<"holy hell this worked.";
                break;
            case 'L':
                gameEngine._curMap.gameOfLife();
                break;
            case 'o':
                gameEngine.displayWallCount();
                break;
            case 'O':
                std::cout<<gameEngine._player->getDistance(0,0)<<std::endl;
                break;
            case 'D':
                gameEngine._player->destruct->damage(gameEngine._player, 1);
                break;
            default:
                break;
        }
    }
    if(gs == gsPlayerMoved)
    {
        if(!moveOrAttack(gameEngine._player, tempX, tempY))
            gs = gsPlayerAct;
        else
            gs = gsPlayerActed;
    }

    return;
}

playerAI::playerAI(pickable* arrow)    :
_arrow(arrow)
{
    ;
}

bool playerAI::moveOrAttack(actor* owner, unsigned newX, unsigned newY)
{
    std::vector<actor*> tItems;
    if(!gameEngine._curMap.getIsWalkable(newX, newY))
    {
        gameEngine.msgQueue.addMessage("Why would you walk into a wall?", TCODColor::gold);
        return false;
    }
    for(actor** iterator = gameEngine._objects.begin(); iterator != gameEngine._objects.end(); iterator++)
    {
        actor* acting = *iterator;
        if(acting->getX() == newX && acting->getY() == newY && owner != acting)
        {
            if(acting->isBlocking)
            {
                if(acting->destruct && !acting->destruct->isDead())
                {
                    //We're attacking, not moving.
                    owner->atker->attack(owner, acting);
                    ai::moveOrAttack(owner, owner->getX(), owner->getY());
                    return true;
                }
                //need to figure where this goes
                gameEngine.msgQueue.addMessage("You can't walk through that.", TCODColor::gold);
                return false;
            }else
            {
                //check to see if its an item
                if(acting->item)
                {
                    tItems.push_back(acting);
                }
            }

        }


    }
    for(unsigned x = 0; x < tItems.size(); x++)
    {
        if( tItems[x]->item->pick(tItems[x], gameEngine._player) )
        {
            ;
        }
    }
    owner->move(newX, newY);
    ai::moveOrAttack(owner, newX, newY);
    return true;

}

void ai::getVector(int & xDistance, int & yDistance, float trueDistance)
{
    if(trueDistance == 0)
    {
        xDistance = 0;
        yDistance = 0;
        return;
    }
    xDistance = int( round( float(xDistance)/trueDistance) );
    yDistance = int( round( float(yDistance)/trueDistance) );
}

void ai::muddyCoords(int & playerX, int & playerY, unsigned intensity)
{
    playerX += gameEngine.rdm->getInt(-1*intensity, intensity);
    playerY += gameEngine.rdm->getInt(-1*intensity, intensity);
    if(playerX > (int)GAMEAREAX)
        playerX = GAMEAREAX;
    if(playerX < 0 )
        playerX = 0;
    if(playerY > (int)GAMEAREAY)
        playerY = GAMEAREAY;
    if(playerY < 0 )
        playerY = 0;
}

explodingCorpseAI::explodingCorpseAI(int ticksStart, int radius)    :
ai(), _ticksTillExploding(ticksStart), _radius(radius)
{
    ;
}

explodingCorpseAI::~explodingCorpseAI()
{
    ;
}

void explodingCorpseAI::update(actor* owner)
{
    if(_ticksTillExploding-- == 0)
    {
        explode(owner);
    }
}

void explodingCorpseAI::explode(actor* owner)
{
    gameEngine.msgQueue.addMessage(owner->name + " has exploded!");
}

changingAI::changingAI(ai* nextAI, unsigned turns, unsigned char nextSymbol, TCODColor nextColor)  :
_nextAI(nextAI), _turns(turns), _nextSymbol(nextSymbol), _nextColor(nextColor)
{
    ;
}

changingAI::~changingAI()
{
    ;
}

void changingAI::update(actor* owner)
{
    if(--_turns <= 0)
    {
        owner->symbol = _nextSymbol;
        owner->foreColor = _nextColor;
        owner->actorAI = _nextAI;
        delete this;
    }
}

antAI::antAI()
{
    _facing = DIRECTIONS( gameEngine.rdm->getInt(1, 4) );
}

void antAI::update(actor* owner)
{
    unsigned tX, tY;
    tX = owner->getX();
    tY = owner->getY();
    //turn
    if(!gameEngine._curMap.getIsWalkable( tX, tY ) )
    {
        //In wall
        if(_facing == dEAST)
            _facing = dNORTH;
        else
        {
            _facing = DIRECTIONS( int(_facing) + 1 );
        }
    //flip wallstate
        gameEngine._curMap.setTileState(tX, tY, gameEngine._curMap.getIsSlimed(tX, tY), true );
    }else
    {
        //On ground
        if(_facing == dNORTH)
            _facing = dEAST;
        else
        {
            _facing = DIRECTIONS( int(_facing) - 1 );
        }

        //flip wallstate
        gameEngine._curMap.setTileState(tX, tY, gameEngine._curMap.getIsSlimed(tX, tY), false );
    }

    //move one square forward
    switch(_facing)
    {
        case dNORTH:
            tY--;
            break;

        case dWEST:
            tX--;
            break;

        case dSOUTH:
            tY++;
            break;

        case dEAST:
            tX++;
            break;
        default:
            break;
    }

    moveOrAttack(owner, tX, tY);
}

bool antAI::moveOrAttack(actor* owner, unsigned newX, unsigned newY)
{
    for(actor** iterator = gameEngine._objects.begin(); iterator != gameEngine._objects.end(); iterator++)
    {
        actor* acting = *iterator;
        if( owner != acting && acting->getX() == newX && acting->getY() == newY)
        {

            if(acting == gameEngine._player && acting->destruct && !acting->destruct->isDead())
            {
                //We're attacking, not moving.
                owner->atker->attack(owner, acting);
                ai::moveOrAttack(owner, owner->getX(), owner->getY());
                return false;
            }
            return false;

        }


    }
    if(newX > GAMEAREAX || newY > GAMEAREAY)
        return false;
    owner->move(newX, newY);
    ai::moveOrAttack(owner, newX, newY);
    return true;
}

bool diggerAI::moveOrAttack(actor* owner, unsigned newX, unsigned newY)
{
    int tX = newX;
    int tY = newY;
    int stepdx = ( tX - int(owner->getX() ) > 0 ? 1:-1);
    int stepdy = ( tY - int(owner->getY() ) > 0 ? 1:-1);

    for(actor** iterator = gameEngine._objects.begin(); iterator != gameEngine._objects.end(); iterator++)
    {
        actor* acting = *iterator;
        if( acting->isBlocking != false && owner != acting && acting->getX() == newX && acting->getY() == newY)
        {

            if(acting == gameEngine._player && acting->destruct && !acting->destruct->isDead())
            {
                //We're attacking, not moving.
                owner->atker->attack(owner, acting);
                ai::moveOrAttack(owner, owner->getX(), owner->getY());
                return false;
            }else
            {
                //we're walking into another unit
                if(gameEngine.isTileClear( (owner->getX() + stepdx), owner->getY(), false ) )
                {
                    owner->move((owner->getX() + stepdx), owner->getY());
                    return true;
                }else
                {
                    if(gameEngine.isTileClear( owner->getX(), (owner->getY() + stepdy ), false ) )
                    {
                        owner->move(owner->getX(), (owner->getY() + stepdy) );
                        return true;
                    }
                }
                gameEngine.msgQueue.addMessage("Walking into another unit, can't slide, diggers dig.");
                return false;
            }

        }
    }
    owner->move(newX, newY);
    ai::moveOrAttack(owner, newX, newY);
    return true;
}

diggerAI::diggerAI(float searchRadius)  :
_searchRadius(searchRadius)
{
    ;
}

void diggerAI::update(actor* owner)
{
    int playerX = gameEngine._player->getX();
    int playerY = gameEngine._player->getY();
    unsigned tempX = owner->getX();
    unsigned tempY = owner->getY();
    int xDistance = 0;
    int yDistance = 0;
    float distanceFromPlayer = gameEngine._player->getDistance(tempX, tempY);

    if(distanceFromPlayer > _searchRadius)
    {
        muddyCoords(playerX, playerY, 2);
        //refigure the distance.
        distanceFromPlayer = gameEngine._player->getDistance(tempX, tempY);
    }

    //find the vectors toward the player
    xDistance = playerX - tempX;
    yDistance = playerY - tempY;
    getVector(xDistance, yDistance, distanceFromPlayer);
    //move toward the player
    tempX += xDistance;
    tempY += yDistance;
    if(tempX >= GAMEAREAX || tempY >= GAMEAREAY)
        std::cout<<"wat";
    if( moveOrAttack(owner, tempX, tempY) )
        gameEngine._curMap.setTileState(tempX, tempY, gameEngine._curMap.getIsSlimed(tempX, tempY), true);
}

bool bullyAI::moveOrAttack(actor* owner, unsigned newX, unsigned newY)
{
    int tX = newX;
    int tY = newY;
    int stepdx = ( tX - int(owner->getX() ) > 0 ? 1:-1);
    int stepdy = ( tY - int(owner->getY() ) > 0 ? 1:-1);
    bool needSlide = false;
    bool isXSame = (owner->getX() == gameEngine._player->getX() );
    bool isYSame = (owner->getY() == gameEngine._player->getY() );

    if( !gameEngine._curMap.getIsWalkable(newX, newY) )
        needSlide = true;

    for(actor** iterator = gameEngine._objects.begin(); iterator != gameEngine._objects.end(); iterator++)
    {
        actor* acting = *iterator;
        if( acting->isBlocking != false && owner != acting && acting->getX() == newX && acting->getY() == newY)
        {

            if(acting == gameEngine._player && acting->destruct && !acting->destruct->isDead() &&
              ( ( isXSame && !isYSame) ||( isYSame && (!isXSame) ) ) )
            {
                //We're attacking, not moving.
                owner->atker->attack(owner, acting);
                ai::moveOrAttack(owner, owner->getX(), owner->getY());
                return false;
            }else
            {
                //we're walking into another unit
                needSlide = true;
            }

        }
    }

    //ran into something, need to slide around it
    if(needSlide)
    {
        if(gameEngine.isTileClear( (owner->getX() + stepdx), owner->getY(), true ) )
        {
            owner->move((owner->getX() + stepdx), owner->getY());
            return true;
        }else
        {
            if(gameEngine.isTileClear( owner->getX(), (owner->getY() + stepdy ), true ) )
            {
                owner->move(owner->getX(), (owner->getY() + stepdy) );
                return true;
            }
        }
        gameEngine.msgQueue.addMessage("Walking into another unit, can't slide, gotta BULLY!");
        return false;
    }
    owner->move(newX, newY);
    ai::moveOrAttack(owner, newX, newY);
    return true;
}

bullyAI::bullyAI(float searchRadius)    :
_searchRadius(searchRadius)
{
    ;
}

void bullyAI::update(actor* owner)
{
    int playerX = gameEngine._player->getX();
    int playerY = gameEngine._player->getY();
    unsigned tempX = owner->getX();
    unsigned tempY = owner->getY();
    int xDistance = 0;
    int yDistance = 0;
    float distanceFromPlayer = gameEngine._player->getDistance(tempX, tempY);

    //check to see if player is in search radius
    if(distanceFromPlayer > _searchRadius)
    {
        //if not, muddy up the player's coords
        muddyCoords(playerX, playerY, 2);
        //refigure the distance.
        distanceFromPlayer = gameEngine._player->getDistance(tempX, tempY);
    }
    //then try and move closer to the coords
    //find the vectors toward the player
    xDistance = playerX - tempX;
    yDistance = playerY - tempY;
    getVector(xDistance, yDistance, distanceFromPlayer);

    //move toward the player
    tempX += xDistance;
    tempY += yDistance;
    moveOrAttack(owner, tempX, tempY);
}

bool tacklerAI::moveOrAttack(actor* owner, unsigned newX, unsigned newY)
{
    ;
}

tacklerAI::tacklerAI()
{
    _haveMoved = false;
}

void tacklerAI::update(actor* owner)
{
    ;
}
/*
void tacklerAI::update(actor* owner)
{
    int playerX = gameEngine._player->getX();
    int playerY = gameEngine._player->getY();
    unsigned tempX = owner->getX();
    unsigned tempY = owner->getY();
    int xDistance = 0;
    int yDistance = 0;
    float distanceFromPlayer = gameEngine._player->getDistance(tempX, tempY);

    //check to see if player is in search radius
    if(distanceFromPlayer > _searchRadius)
    {
        //if not, muddy up the player's coords
        muddyCoords(playerX, playerY, 2);
        //refigure the distance.
        distanceFromPlayer = gameEngine._player->getDistance(tempX, tempY);
    }
    //then try and move closer to the coords
    //find the vectors toward the player
    xDistance = playerX - tempX;
    yDistance = playerY - tempY;
    getVector(xDistance, yDistance, distanceFromPlayer);

    //move toward the player
    tempX += xDistance;
    tempY += yDistance;
    moveOrAttack(owner, tempX, tempY);
}
*/
