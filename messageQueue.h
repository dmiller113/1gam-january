#ifndef MESSAGEQUEUE_H
#define MESSAGEQUEUE_H


struct message
{
    std::string text;
    TCODColor clr;
};

class messageQueue
{
private:
    unsigned size;
    message msgQueue[MESSAGEQUEUEY];
public:
    messageQueue();
    ~messageQueue();
    void addMessage(std::string txt, TCODColor clr = TCODColor::white);
    message getMessage(unsigned pos = 0) const;
    unsigned getSize(void) const {return size;}
    void emptyQueue(void);
protected:
};

#endif // MESSAGEQUEUE_H
