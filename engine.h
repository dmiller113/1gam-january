#ifndef ENGINE_H
#define ENGINE_H

#include <vector>

class monsterFactory;

class engine
{
private:

    unsigned _score; //Player's score
    unsigned _round;
    unsigned _tick;
    unsigned _curFps;
    unsigned _time;
    unsigned _lastTime;
    unsigned GOLTICK;
    unsigned _spawnTick;
    MONSTERTYPE _early[10];
    MONSTERTYPE _mid[10];
    MONSTERTYPE _late[10];
    MONSTERTYPE _last[10];

public:

    unsigned _arrowCharge;
    unsigned _maxCharge;
    //pointer to the player
    actor* _player;
    //pointer to the map
    map _curMap;
    //TCODList containing all the objects in the game
    TCODList<actor*> _objects;
    messageQueue msgQueue;
    monsterFactory* monFactory;
    TCODRandom* rdm;
private:
    MONSTERTYPE determineSpawnType(unsigned spawnPoints);
    void varyRoundSpawns(void);
public:
    engine();
    virtual ~engine();
    void init(void);
    void cleanup(void);
    void gameTick(void);
    void render(void);
    void drawUI(TCODConsole* console);
    void drawMessage(TCODConsole* console);
    void drawGameboard(TCODConsole* console);
    void displayWallCount(void);
    void sendToBack(actor* object);
    void addScore(unsigned points);
    void normalRoundSpawn(void);
    bool isTileClear(unsigned tX, unsigned tY, bool wallsBlock = true);
};

extern engine gameEngine;

struct monsterDefinition
{
    MONSTERTYPE mType;
    std::string name;
    unsigned char symbol;
    TCODColor fColor;
    TCODColor bColor;
    unsigned hp;
    unsigned shield;
    unsigned damage;
    unsigned points;
};

class monsterFactory
{
    std::vector<monsterDefinition> _definitions;

private:
    ai* createAI(MONSTERTYPE type, unsigned definitionNum, bool isEgg = false);

public:
    monsterFactory(std::vector<monsterDefinition> definition);
    actor* createMonster(MONSTERTYPE type, unsigned cX, unsigned cY, bool isEgg = false);
    unsigned getPoints(MONSTERTYPE type) const;
};

#endif // ENGINE_H
