#include "main.h"
#include <cmath>

actor::actor()
{
    //ctor
    _x = 0;
    _y = 0;
    name = "undefined";
    description = "unknown";
    symbol = '@';
    foreColor = TCODColor::white;
    backColor = TCODColor::black;
    isBlocking = true;
    atker = NULL;
    destruct = NULL;
    actorAI = NULL;
    item = NULL;
    itemsHeld = NULL;
}

actor::actor(unsigned x, unsigned y, std::string nm, std::string desc, int ch, TCODColor fColor, TCODColor bColor, bool blocks)
{
    //probably going to be the most used actor constructor.
    _x = x;
    _y = y;
    name = nm;
    symbol = ch;
    foreColor = fColor;
    backColor = bColor;
    isBlocking = blocks;
    description = desc;
    atker = NULL;
    destruct = NULL;
    actorAI = NULL;
    item = NULL;
    itemsHeld = NULL;
}

actor::~actor()
{
    //dtor

    if(atker)
    {
        delete atker;
    }
    if(destruct)
    {
        delete destruct;
    }
    if(actorAI)
    {
        delete actorAI;
    }
    if(item)
    {
        delete item;
    }
    if(itemsHeld)
    {
        delete itemsHeld;
    }
}

void actor::update(void)
{
    if(actorAI)
    {
        actorAI->update(this);
    }
    if(destruct)
    {
        ;//destruct->update();
    }
    return;
}

void actor::move(unsigned newX, unsigned newY)
{
    //helper function and function that movement abilities will use to move actors. Has no checks for being able to be in the tile.
    _x = newX;
    _y = newY;
}

void actor::render(TCODConsole* drawable) const
{
    //draw this sucka!
    drawable->putCharEx(_x, _y, symbol, foreColor, backColor);
}

std::string actor::returnDescription(void)
{
    /*
    if(item == NULL || item->isID())
    {
        return description;
    }else
    {
        return item->falseDescription;
    }
    */
    return name;
}

float actor::getDistance(unsigned x2, unsigned y2)
{
    unsigned length = x2 - _x;
    unsigned height = y2 - _y;
    float distance = (length * length) + (height * height);
    distance = sqrt(distance);

    return distance;

}
