#ifndef EFFECTS_H
#define EFFECTS_H

class targetSelection
{
protected:
    float _range;
    float _areaOfEffect;
    bool _isSquare;
public:
    targetSelection(float range, float AoE, bool square = false);
    void selectTargets(actor* user, TCODList<actor*> & objectList, DIRECTIONS angle);
    float getRange(void) const {return _range;}
};

class effect
{
public:
    effect();
    virtual ~effect();
    virtual bool applyTo(actor* actor) = 0;
};

class testEffect : public effect
{
public:
    testEffect();
    bool applyTo(actor* effected);
};

class healthEffect  :   public effect
{
    int _amount;
    const std::string _message;
    bool _overHealOK;
public:
    healthEffect(std::string message, int amount = 1, bool _overHealOK = false);
    bool applyTo(actor* owner);
};

#endif // EFFECTS_H
