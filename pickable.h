#ifndef PICKABLE_H
#define PICKABLE_H


class container
{
private:
    TCODList<actor *> inventory;
    int size; //0 is unlimited, otherwise max size
public:
    container(int cSsize = 0);
    ~container();
    int getSize(void)const {return size;}
    bool add(actor* object);
    void remove(actor* object);
    actor* getItem(int position = 0);
};

class pickable
{
protected:
    unsigned _charges;
    //target select for what actors get effected by the item's effect
    targetSelection* _targetSelector;
    //effect of the item
    effect* _effect;
private:
    DIRECTIONS getDirection(void);
public:
    pickable();
    pickable(unsigned charges = 1, targetSelection* TS = NULL, effect* eff = NULL); //the rest of this will be to place the target selector and effect
    virtual ~pickable();
    bool pick(actor* owner, actor* wearer);
    bool use(actor* owner, actor* user);

};

#endif // PICKABLE_H
