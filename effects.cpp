#include "main.h"

targetSelection::targetSelection(float range, float aoe, bool isSquare)    :
_range(range), _areaOfEffect(aoe), _isSquare(isSquare)
{
    ;
}

void targetSelection::selectTargets(actor* user, TCODList<actor*> & objectList, DIRECTIONS angle)
{
    unsigned effectX, effectY;
    effectX = 0;
    effectY = 0;
    unsigned tX = 0;
    unsigned tY = 0;

    float distance = _range;

    int xModifier = 0;
    int yModifier = 0;

    switch(angle)
    {
        case dSELF:
        {
        //this is an effect is centered on yourself
            effectX = user->getX();
            effectY = user->getY();
            break;
        }
        case dNORTH:
        {
            //set the modifier to y coord to make it faster to sort
            yModifier = -1;  //the object's y will be smaller. Fix that.
            break;
        }
        case dSOUTH:
        {
            //set the modifier to y
            yModifier = 1;
            break;
        }
        case dWEST:
        {
            //set the modifier for x. The objects x will be smaller. Fix it.
            xModifier = -1;
            break;
        }
        case dEAST:
        {
            //set the modifier for x.
            xModifier = 1;
            break;
        }
        case dERROR:
        {
            return;
            break;
        }
    }

    effectX = gameEngine._player->getX() + (_range * xModifier);
    effectY = gameEngine._player->getY() + (_range * yModifier);

    //find the final x,y coords for the effect center
    //see if there's any walls in the way.
    for(unsigned j = 1; j <= _range; j++ )
    {
        tX = (gameEngine._player->getX() + (j * xModifier) );
        tY = (gameEngine._player->getY() + (j * yModifier) );
        if( !gameEngine._curMap.getIsWalkable( tX, tY ) )
        {

            if( gameEngine._player->getDistance(tX, tY) < distance)
            {
                effectX = gameEngine._player->getX() + (j * xModifier);
                effectY = gameEngine._player->getY() + (j * yModifier);
                distance = gameEngine._player->getDistance(effectX, effectY);
            }
            break;
        }
    }

    //find out if it hits an object before the max range

    for(actor** it = gameEngine._objects.begin(); it != gameEngine._objects.end(); it++)
    {
        actor* object = *it;
        if(!object->isBlocking)
            continue;
        if(object == user)
            continue;
        tX = object->getX();
        tY = object->getY();
        if(angle == dEAST || angle == dWEST)
        {
            //Only objects with the same Y can possibly be considered
            if(tY != user->getY())
                continue;
            if(angle == dEAST && tX < user->getX())
                continue;
            if(angle == dWEST && tX > user->getX())
                continue;
        }else
        {
            //its a dNORTH dSOUTH object, so only objects with the same X matters
            if(tX != user->getX())
                continue;
            if(angle == dNORTH && tY > user->getY())
                continue;
            if(angle == dSOUTH && tY < user->getY())
                continue;
        }
        if(distance > gameEngine._player->getDistance(tX, tY))
        {
            distance = gameEngine._player->getDistance(tX, tY);
            effectX = tX;
            effectY = tY;
        }
    }

    //find all the objects effected
    for (actor **iterator = gameEngine._objects.begin(); iterator != gameEngine._objects.end(); iterator++)
    {
        actor* obj = *iterator;
        if (  obj->destruct && !obj->destruct->isDead()
            && obj->getDistance(effectX, effectY) <= _areaOfEffect) {
            objectList.push(obj);
        }
    }
}

effect::effect()
{
    //ctor
}

effect::~effect()
{
    //dtor
}

testEffect::testEffect()
{
    ;
}

bool testEffect::applyTo(actor* effected)
{
    gameEngine.msgQueue.addMessage(effected->name + " effected!");
    return true;
}

healthEffect::healthEffect(std::string message, int amount, bool overHeal)  :
_amount(amount), _message(message), _overHealOK(overHeal)
{
    ;
}

bool healthEffect::applyTo(actor* owner)
{
    bool succeed = false;
    TCODColor msgColor = TCODColor::flame;
    if(_amount < 0)
    {
        msgColor = TCODColor::green;
    }
    if(owner->destruct)
    {
        gameEngine.msgQueue.addMessage(owner->name + _message, msgColor);
        if(_amount > 0)
        {
            owner->destruct->damage(owner, _amount);
            succeed = true;
        }else
        {
            if(owner->destruct->heal(owner, (-1 * _amount), _overHealOK))
                succeed = true;
        }
    }

    return succeed;
}
