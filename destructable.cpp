#include "main.h"

destructable::destructable()
{
    //ctor
    _shieldAmount = 1;
    _maxShield = 1;
    _hp = 1;
    _maxHP = 1;
    _shieldRecharge = 5;
    _regen = 5;
    _regenTick = 0;
    _shieldTick = 0;
}

destructable::destructable(int shieldAmount, int shieldRecharge, int HP, int regen) :
_shieldAmount(shieldAmount), _maxShield(shieldAmount), _hp(HP), _maxHP(HP), _shieldTick(shieldRecharge), _shieldRecharge(shieldRecharge), _regenTick(regen),  _regen(regen)
{
    ;
}

destructable::~destructable()
{
    //dtor
}

bool destructable::damage(actor* owner, int amount)
{
    if(_shieldAmount - amount > 0)
    {
        //the shield can take it! Only does shield damage.
        _shieldAmount -= amount;
    }else
    {
        //the shield is down! Too much damage. But it still stops you from taking HP damage for this attack.
        if(_shieldAmount > 0)
        {
            //We had shield to begin with, so shield down, but no HP damage.
            _shieldAmount = 0;
            //need to start the ticks for regen
            _shieldTick = 0;
            gameEngine.msgQueue.addMessage(owner->name + "'s shield has overloaded!", TCODColor::lightBlue);
        }else
        {
            //shields were down when we took damage! HP damage has occured
            if(_hp - amount > 0)
            {
                //Woo! We didn't die. Gotta take damage though
                _hp -= amount;
                //Set up the HP regen tick
                _regenTick = 0;
            }else
            {
                //Fawk, dead. Time to call die(), take care of corpse dropping, and messaging
                _hp = 0;
                _regenTick = 0;
                gameEngine.msgQueue.addMessage(owner->name + " has died.");
                die(owner);
                return true;
            }
        }
    }
    return false;
}

void destructable::update(void)
{
    ;
}

unsigned destructable::attacked(actor* attackee, actor* attacker, unsigned amount)
{
    int tDamage = 0;
    //getting attacked.
    //Might be a boss mod that needs to trigger on that
    ;//space for that mod
    //so damage them
    tDamage = damage(attackee, amount);
    if(tDamage > 0)
        ;//space for on damage abilities
    return tDamage;
}

bool destructable::heal(actor* owner, int amount, bool overHealOK)
{
    if(overHealOK || ( (amount + _hp) < _maxHP))
    {
        _hp += amount;
        return true;
    }
    return false;
}

void playerDestructable::update(void)
{
    if(_shieldAmount < _maxShield)
    {
        //We're under max shield, try and regen it.
        if(_shieldTick < _shieldRecharge)
        {
            //not at the shield regen number yet
            _shieldTick++;
            if(_shieldTick == _shieldRecharge)
            {
                //shield regen turn. Up the shield
                _shieldAmount++;
                gameEngine.msgQueue.addMessage("Your shield returns!", TCODColor::lightBlue);
            }
        }

    }
    if(gameEngine._arrowCharge < gameEngine._maxCharge)
    {
        gameEngine._arrowCharge++;
    }
}

playerDestructable::playerDestructable(int shieldAmount, int shieldRecharge, int HP, int regen) :
destructable(shieldAmount, shieldRecharge, HP, regen)
{
    ;
}

bool destructable::isDead(void) const
{
    return (_hp <= 0);
}

void destructable::die(actor* owner)
{
    owner->symbol = '%';
    owner->foreColor = TCODColor::red;
    owner->name.insert(0, "Corpse of ");
    owner->isBlocking = false;
    gameEngine.sendToBack(owner);
}

void playerDestructable::die(actor* owner)
{
    gs = gsDead;
    destructable::die(owner);
}

monsterDestructable::monsterDestructable(int shieldAmount, int shieldRecharge, int HP, int regen, int pointsWorth, ai* corpse = NULL)    :
_pointValue(pointsWorth), corpseAI(corpse), destructable(shieldAmount, shieldRecharge, HP, regen)
{
    ;
}

monsterDestructable::~monsterDestructable(void)
{
    if(corpseAI)
    {
        delete corpseAI;
    }
}

void monsterDestructable::update(void)
{
    ;
}

void monsterDestructable::die(actor* owner)
{
    //need to get rid of the old AI for the corpse AI
    if(owner->actorAI)
        delete owner->actorAI;
    owner->actorAI = corpseAI;
    //set corpseAI to NULL so that we don't delete it twice
    corpseAI = NULL;

    //Add the monsters point value to the player's score
    gameEngine.addScore(_pointValue);

    //check to see if we dropped an item/upgrade
    ;

    destructable::die(owner);
}
