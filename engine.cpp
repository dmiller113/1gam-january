#include "main.h"


//Definition for ANT
const monsterDefinition ANT =
    { mANT, "Ant", 'A', TCODColor::lightAmber, TCODColor::black, 1, 0, 1, 5 };

//definition for BULLY
const monsterDefinition BULLY =
    { mBULLY, "Bully", 'B', TCODColor::sea, TCODColor::black, 2, 0, 1, 20 };

//definition for DIGGER
const monsterDefinition DIGGER =
    { mDIGGER, "Digger", 'D', TCODColor::chartreuse, TCODColor::black, 1, 0, 1, 10 };

//definition for PULSERD
const monsterDefinition PULSERD =
    { mPULSERD, "Pulser", 'P', TCODColor::azure, TCODColor::black, 1, 0, 1, 10 };

//definition for PULSERO
const monsterDefinition PULSERO =
    { mPULSERO, "Pulser", 'P', TCODColor::flame, TCODColor::black, 1, 0, 1, 10 };

//definition for SHOOTER
const monsterDefinition SHOOTER =
    { mSHOOTER, "Shooter", 'S', TCODColor::copper, TCODColor::black, 1, 0, 1, 15 };

//definition for TACKLER
const monsterDefinition TACKLER =
    { mTACKLE, "Tackler", 'T', TCODColor::fuchsia, TCODColor::black, 1, 0, 1, 25 };

//definition for WURM
const monsterDefinition WURM =
    { mWURM, "Wurm", 'W', TCODColor::lime, TCODColor::black, 1, 0, 1, 10 };

//definition for MIDBOSS base
const monsterDefinition MIDBOSS =
    { mMIDBOSS, "MidBoss", 'M', TCODColor::lightFuchsia, TCODColor::black, 3, 0, 1, 100 };

const MONSTERTYPE early[10] = {mSHOOTER, mBULLY, mBULLY, mBULLY, mBULLY, mSHOOTER, mSHOOTER, mSHOOTER, mDIGGER, mDIGGER};
const MONSTERTYPE mid[10] = {mSHOOTER, mDIGGER, mBULLY, mBULLY, mBULLY, mSHOOTER, mSHOOTER, mPULSERD, mPULSERO, mWURM};
const MONSTERTYPE late[10] = {mDIGGER, mDIGGER, mDIGGER, mBULLY, mBULLY, mSHOOTER, mPULSERD, mPULSERO, mTACKLE, mSHOOTER};
const MONSTERTYPE last[10] = {mTACKLE, mWURM, mBULLY, mBULLY, mTACKLE, mTACKLE, mSHOOTER, mPULSERD, mPULSERO, mDIGGER};

engine::engine()
{
    //ctor
    _score = 0;
    _round = 1;
    _tick = 0;
    _time = 1;
    _curFps = 0;
    _player = NULL;
    monFactory = NULL;
}

engine::~engine()
{
    //dtor
}

void engine::init(void)
{
    _score = 0;
    _round = 1;
    _tick = 3;  //(round + 2)
    _time = 1;
    _curFps = 0;
    _arrowCharge = 5;
    _maxCharge = 5;
    GOLTICK = 16;
    _spawnTick = 6;
    rdm = new TCODRandom();
    std::vector<monsterDefinition> mDefs;
    mDefs.push_back(ANT);
    mDefs.push_back(BULLY);
    mDefs.push_back(DIGGER);
    mDefs.push_back(PULSERD);
    mDefs.push_back(PULSERO);
    mDefs.push_back(SHOOTER);
    mDefs.push_back(TACKLER);
    mDefs.push_back(WURM);
    mDefs.push_back(MIDBOSS);
    monFactory = new monsterFactory(mDefs);
    varyRoundSpawns();
    _player = new actor(20,20, "player", "looking rather @ today", '@' );
    _player->actorAI = new playerAI( new pickable(1, new targetSelection(6, 0, true), new healthEffect(" is struck by an Arrow!") ) );
    _player->destruct = new playerDestructable(1, 6, 1, 6);
    _player->atker = new attacker(1);
    _objects.push(_player);
    //normalRoundSpawn();
    actor* aTest = monFactory->createMonster(mDIGGER, 24, 20, true);
    _objects.push(aTest);
    aTest = monFactory->createMonster(mBULLY, 22, 20, true);
    _objects.push(aTest);
    actor* itemTest = new actor(29, 29, "Upgrade", "a melee upgrade", '*', TCODColor::red, TCODColor::black, false);
    itemTest->item = new pickable(0);
    _objects.push(itemTest);
    _lastTime = 0;
    _curMap.createMap();
}

void engine::cleanup(void)
{
    if(_objects.size() != 0)
        _objects.clearAndDelete();
    _player = NULL;
    msgQueue.emptyQueue();
    if(monFactory)
    {
        delete monFactory;
    }
    if(rdm)
        delete rdm;
}

void engine::render(void)
{
    //consoles for the various parts of the game window
    TCODConsole* cUI = new TCODConsole(WINDOWX - 2, UI);
    TCODConsole* msg = new TCODConsole(WINDOWX - 2, MESSAGEQUEUEY);
    TCODConsole* game = new TCODConsole(GAMEAREAX, GAMEAREAY);

    //draw each console

    //draw the game area
    drawGameboard(game);
    //draw the UI
    drawUI(cUI);
    //draw the last 5 messages
    drawMessage(msg);


    //now blit all that to the root console
    TCODConsole::blit(game, 0, 0, 0, 0, TCODConsole::root, GAMEBOARDT1 + 1, 1);
    TCODConsole::blit(msg, 0, 0, 0, 0, TCODConsole::root, 1, GAMEAREAY+BORDER+UI - 1);
    TCODConsole::blit(cUI, 0, 0, 0, 0, TCODConsole::root, 1, GAMEAREAY+BORDER - 1);
    //figure fps
    ;

    //clean up.
    delete cUI;
    delete msg;
    delete game;
}

void engine::gameTick(void)
{
    _lastTime = _time;
    if(gs == gsPlayerMoved || gs == gsPlayerActed)
    {
        gs = gsPlayerAct;
        _time += 1;
        _player->destruct->update();
    }
    //handle the player's input
    _player->update();
    //then handle everyone elses
    if(gs == gsPlayerMoved || gs == gsPlayerActed)
    {
        for(actor** it = _objects.begin(); it != _objects.end(); it++)
        {
            actor* acting = *(it);
            if(acting != _player)
                acting->update();
        }
    }
    if(_time%(GOLTICK - _round) == 0 && (_lastTime != _time) )
    {
        _curMap.gameOfLife();
    }
    if(_time % _spawnTick == 0 && (_lastTime != _time) )
    {
        normalRoundSpawn();
        _tick--;
        if(_tick <= 0)
        {
            msgQueue.addMessage("Spawn a boss this turn");
            _round++;
            _tick = (_round + 2);
        }
        //plan: After 9th round, disable boss spawning and spawn a Master of the Arena. If the player kills that monster, they win.
    }
}

void engine::drawGameboard(TCODConsole* console)
{
    //draw the map
    TCODColor tileColor = TCODColor::black;

    for(unsigned x = 0; x < GAMEAREAX; x++)
    {
        for(unsigned y = 0; y < GAMEAREAY; y++)
        {
            if(_curMap.getIsSlimed(x, y))
                tileColor = TCODColor::green;
            console->putCharEx(x, y, _curMap.getTileSymbol(x,y), TCODColor::lightestGrey, tileColor);
            tileColor = TCODColor::black;
        }
    };

    //draw the objects on the map
    for(actor** it = _objects.begin(); it != _objects.end(); it++)
    {
        actor* acting = *it;
            if( acting != _player)
            {
                acting->render(console);
            }
    }

    //draw the player
    _player->render(console);
}

void engine::displayWallCount(void)
{
    unsigned wallcount = 0;
    TCODColor tcolor = TCODColor::red;
    TCODConsole* temp = new TCODConsole(GAMEAREAX,GAMEAREAY);

    for(unsigned x = 0; x < GAMEAREAX; x++)
    {
        for(unsigned y = 0; y < GAMEAREAY; y++)
        {
            wallcount = _curMap.getWallCount(x,y);
            if(wallcount == 0)
                tcolor = TCODColor::black;
            if(wallcount > 2)
                tcolor = TCODColor::orange;
            if(wallcount > 5)
                tcolor = TCODColor::yellow;
            if(wallcount == 8)
                tcolor = TCODColor::white;
            temp->putCharEx(x,y, ('0'+wallcount), tcolor, TCODColor::black);
            tcolor = TCODColor::lightRed;
        }
    }
    TCODConsole::blit(temp, 0, 0, GAMEAREAX, GAMEAREAY, TCODConsole::root, GAMEBOARDT1 +1, 1);
    TCODConsole::root->flush();
    TCOD_key_t key = TCODConsole::waitForKeypress(true);
    key = TCODConsole::waitForKeypress(true);
}

void engine::drawMessage(TCODConsole* console)
{
        //draw dem messages
    for(unsigned x = 0; x < msgQueue.getSize(); x++)
    {

        console->setDefaultForeground(msgQueue.getMessage(x).clr);
        console->print(0, x, msgQueue.getMessage(x).text.c_str());
    }
    return;
}

void engine::drawUI(TCODConsole* console)
{
    unsigned char shield[] = {'S', 'h', 'i', 'e', 'l', 'd'};
    unsigned char health[] = {'H', 'e', 'a', 'l', 't', 'h'};
    unsigned char fire[] = {'F', 'i', 'r', 'e', '!'};
    int count = 0;
    int numLetters = ( float(_player->destruct->_shieldTick)/float(_player->destruct->_shieldRecharge) * float(6));
    //time to display the words shield and health. They'll be colored letter by letter
    for(int x = 0; x < (numLetters); x++)
    {
        console->putCharEx(x + GAMEBOARDT1, 1, shield[x], TCODColor::lightBlue, TCODColor::black);
        count++;
    }
    for(int y = count; y < 6; y++)
    {
        console->putCharEx(y + GAMEBOARDT1, 1, shield[y], TCODColor::white, TCODColor::black);
    }

    //time for health
    numLetters = ( float(_player->destruct->_regenTick)/float(_player->destruct->_regen) * float(6));
    count = 0;
    for(int a = 0; a < (numLetters); a++)
    {
        console->putCharEx(a + GAMEBOARDT1 + 14, 1, health[a], TCODColor::red, TCODColor::black);
        count++;
    }
    for(int b = count; b < 6; b++)
    {
        console->putCharEx(b + GAMEBOARDT1 + 14, 1, health[b], TCODColor::white, TCODColor::black);
    }

    numLetters = ( float(_arrowCharge)/float(_maxCharge) * float(5));
    //Time for Fire
    count = 0;
    for(int a = 0; a < (numLetters); a++)
    {
        console->putCharEx(a + GAMEBOARDT1 + 27, 1, fire[a], TCODColor::lightGreen, TCODColor::black);
        count++;
    }
    for(int b = count; b < 5; b++)
    {
        console->putCharEx(b + GAMEBOARDT1 + 27, 1, fire[b], TCODColor::white, TCODColor::black);
    }

    //time to display the round, spawns till miniboss, turns total and score.
    console->print(GAMEBOARDT1 + 39, 1, "Score: %d", _score);
    console->print(GAMEBOARDT1, 2, "Round: %d", _round);
    console->print(GAMEBOARDT1 + 12, 2, "MiniBoss: %d", _tick);
    console->print(GAMEBOARDT1 + 27, 2, "Time: %d", _time);
}

void engine::sendToBack(actor* object)
{
    _objects.remove(object);
    _objects.insertBefore(object, 0);
}

void engine::addScore(unsigned points)
{
    _score += points;
}

void engine::normalRoundSpawn(void)
{
    std::vector<MONSTERTYPE> spawnGroup;
    unsigned spawnPoints = _round * 60 + ( (_round + 2) * 10) - (_tick * 10);
    MONSTERTYPE type;
    bool isMass = false;
    unsigned num = 0;
    unsigned spawnRadius = 28;
    float minSpawnRadius = 8.0;
    unsigned rX = 0;
    unsigned rY = 0;
    unsigned attempts = 0;

    msgQueue.addMessage("More egg pods swell up from the floor!", TCODColor::lightFlame);
    //Determine if this is a MASS spawn.
    if(rdm->getInt(1, 100) < _round)
    {
        isMass = true;  //% chance of mass spawns is equal to the round number, capping at 9% at round 9.
        msgQueue.addMessage("There's so many of them!", TCODColor::lightFlame);
    }
    while(spawnPoints > 0)
    {
        //Determine what monster type to spawn.
        type = determineSpawnType(spawnPoints);
        //Determine how many monsters of that type to spawn.
        if(isMass)
        {
            if(spawnPoints < 25)
                type = mANT;    //handle the remaining amount after the first mass spawn.
            num = spawnPoints/monFactory->getPoints(type);
        }else
        {
            if(spawnPoints < 25)
                isMass = true;    //handle cases where there isn't enough points for a monster
            //No group can be over 2/3 the points remaining before the third group, or if a MASS spawn is triggered
            num = ( (spawnPoints/5) * 3) / monFactory->getPoints(type);
        }

        for(unsigned y = 0; y < num; y++)
        {
            spawnGroup.push_back(type);
        }

        if(num == 0)
        {
            num = 1;
            spawnPoints = 25;
        }
        spawnPoints -= num * monFactory->getPoints(type);
        if(spawnPoints < 0)
            spawnPoints = 0;
        //if not a MASS spawn, the first group can be a champion spawn
        ;
        //Determine location of spawns. Monsters shouldn't be terribly far from the player, but should not spawn within 8 tiles of them either.

        spawnRadius -= _round * 2;
        //Spawn monsters.

        for(unsigned x = 0; x < spawnGroup.size(); x++)
        {
            do
            {
                rX = 0;
                rY = 0;
                if(++attempts > 99)
                {
                    if(spawnRadius > 28)
                        break;
                    spawnRadius += 5;
                    attempts = 0;
                }
                if(minSpawnRadius > 2.0 && rdm->getInt(0,9) == 7)
                    minSpawnRadius -= 2.0;

                rX = rdm->getInt( (-1 * spawnRadius), spawnRadius);
                rY = rdm->getInt( (-1 * spawnRadius), spawnRadius);
                rX += _player->getX();
                rY += _player->getY();
                if(rX < 0)
                    rX = 0;
                if(rY < 0)
                    rY = 0;
                if(rX > GAMEAREAX - 1 )
                    rX = GAMEAREAX - 1;
                if(rY > GAMEAREAY - 1)
                    rY = GAMEAREAY - 1;
            }while( (_player->getDistance(rX, rY) < minSpawnRadius) || (!isTileClear(rX, rY) ) );
            _objects.push( monFactory->createMonster(spawnGroup[x], rX, rY, true ) );

        }
        spawnGroup.clear();
    }



}

MONSTERTYPE engine::determineSpawnType(unsigned spawnPoints)
{

    MONSTERTYPE mType = mBULLY;
    int randomNum = rdm->getInt(0, 10);

    switch (_round)
    {
        case 1:
        case 2:
            mType = _early[randomNum];
            break;
        case 3:
        case 4:
        case 5:
            mType = _mid[randomNum];
            break;
        case 6:
        case 7:
        case 8:
            mType = _late[randomNum];
            break;
        case 9:
            mType = _last[randomNum];
            break;
    }

    return mType;
}

void engine::varyRoundSpawns(void)
{
    unsigned randomNumber = 0;
    unsigned randomTypeNum = 0;

    for(unsigned x = 0; x < 10; x++)
    {
        _early[x] = early[x];
        _mid[x] = mid[x];
        _late[x] = late[x];
        _last[x] = last[x];
    }

    //modify each round spawn list
    for(unsigned y = 0; y < 2; y++)
    {
        randomNumber = rdm->getInt(0, 9);
        randomTypeNum = rdm->getInt(0, 7);
        switch(randomTypeNum)
        {
            case 0:
                //ant
                //_early[randomNumber] = mANT;
                break;
            case 1:
                //bully
                _early[randomNumber] = mBULLY;
                break;
            case 2:
                //digger
                _early[randomNumber] = mDIGGER;
                break;
            case 3:
                //pulser
                _early[randomNumber] = mPULSERO;
                break;
            case 4:
                //pulser diagnal
                _early[randomNumber] = mPULSERD;
                break;
            case 5:
                //shooter
                _early[randomNumber] = mSHOOTER;
                break;
            case 6:
                //tackler
                _early[randomNumber] = mTACKLE;
                break;
            case 7:
                //wurm
                _early[randomNumber] = mWURM;
                break;
            default:
                break;
        }
    }

    //mid
    for(unsigned y = 0; y < 2; y++)
    {
        randomNumber = rdm->getInt(0, 9);
        randomTypeNum = rdm->getInt(0, 7);
        switch(randomTypeNum)
        {
            case 0:
                //ant
                _mid[randomNumber] = mANT;
                break;
            case 1:
                //bully
                _mid[randomNumber] = mBULLY;
                break;
            case 2:
                //digger
                _mid[randomNumber] = mDIGGER;
                break;
            case 3:
                //pulser
                _mid[randomNumber] = mPULSERO;
                break;
            case 4:
                //pulser diagnal
                _mid[randomNumber] = mPULSERD;
                break;
            case 5:
                //shooter
                _mid[randomNumber] = mSHOOTER;
                break;
            case 6:
                //tackler
                _mid[randomNumber] = mTACKLE;
                break;
            case 7:
                //wurm
                _mid[randomNumber] = mWURM;
                break;
            default:
                break;
        }
    }

    //late
    for(unsigned y = 0; y < 2; y++)
    {
        randomNumber = rdm->getInt(0, 9);
        randomTypeNum = rdm->getInt(0, 7);
        switch(randomTypeNum)
        {
            case 0:
                //ant
                _late[randomNumber] = mANT;
                break;
            case 1:
                //bully
                _late[randomNumber] = mBULLY;
                break;
            case 2:
                //digger
                _late[randomNumber] = mDIGGER;
                break;
            case 3:
                //pulser
                _late[randomNumber] = mPULSERO;
                break;
            case 4:
                //pulser diagnal
                _late[randomNumber] = mPULSERD;
                break;
            case 5:
                //shooter
                _late[randomNumber] = mSHOOTER;
                break;
            case 6:
                //tackler
                _late[randomNumber] = mTACKLE;
                break;
            case 7:
                //wurm
                _late[randomNumber] = mWURM;
                break;
            default:
                break;
        }
    }

    //last
    for(unsigned y = 0; y < 2; y++)
    {
        randomNumber = rdm->getInt(0, 9);
        randomTypeNum = rdm->getInt(0, 7);
        switch(randomTypeNum)
        {
            case 0:
                //ant
                _last[randomNumber] = mANT;
                break;
            case 1:
                //bully
                _last[randomNumber] = mBULLY;
                break;
            case 2:
                //digger
                _last[randomNumber] = mDIGGER;
                break;
            case 3:
                //pulser
                _last[randomNumber] = mPULSERO;
                break;
            case 4:
                //pulser diagnal
                _last[randomNumber] = mPULSERD;
                break;
            case 5:
                //shooter
                _last[randomNumber] = mSHOOTER;
                break;
            case 6:
                //tackler
                _last[randomNumber] = mTACKLE;
                break;
            case 7:
                //wurm
                _last[randomNumber] = mWURM;
                break;
            default:
                break;
        }
    }
}

bool engine::isTileClear(unsigned tX, unsigned tY, bool wallsBlock)
{
    bool isClear;
    if(wallsBlock)
        isClear = _curMap.getIsWalkable(tX, tY);
    else
        isClear = true;

    if(isClear)
    {
        for(actor** it = _objects.begin(); it != _objects.end(); it++ )
        {
            if((*it)->isBlocking && (*it)->getX() == tX && tY == (*it)->getY() )
            {
                isClear = false;
                break;
            }
        }
    }
    return isClear;
}

monsterFactory::monsterFactory(std::vector<monsterDefinition> definition)
{
    for(unsigned x = 0; x < definition.size(); x++)
    {
        _definitions.push_back(definition[x]);
    }
}

actor* monsterFactory::createMonster(MONSTERTYPE type, unsigned cX, unsigned cY, bool isEgg)
{
    actor* returnActor = NULL;

    for(unsigned x = 0; x < _definitions.size(); x++)
    {
        if(_definitions[x].mType == type)
        {
            returnActor = new actor(cX, cY, _definitions[x].name, _definitions[x].name, _definitions[x].symbol, _definitions[x].fColor, _definitions[x].bColor, true);
            returnActor->atker = new attacker(_definitions[x].damage);
            returnActor->destruct = new monsterDestructable(_definitions[x].shield, 0, _definitions[x].hp, 0, _definitions[x].points, new explodingCorpseAI(3, 1));
            returnActor->actorAI = createAI(type, x, isEgg);
        }
    }

    if(isEgg)
    {
        returnActor->symbol = '!';
        returnActor->foreColor = TCODColor::orange;
    }
    return returnActor;
}

ai* monsterFactory::createAI(MONSTERTYPE type, unsigned definitionNum, bool isEgg)
{
    ai* returnAI = NULL;
    ai* eggAI = NULL;
    unsigned nextSymbol = _definitions[definitionNum].symbol;
    TCODColor nextColor = _definitions[definitionNum].fColor;

    switch(type)
    {
        case mANT:
        {
            returnAI = new antAI();
            break;
        }
        case mBULLY:
        {
            returnAI = new bullyAI(6.0);
            break;
        }
        case mDIGGER:
        {
            returnAI = new diggerAI(6.0);
            break;
        }
        case mPULSERD:
        {
            break;
        }
        case mPULSERO:
        {
            break;
        }
        case mSHOOTER:
        {
            break;
        }
        case mTACKLE:
        {
            break;
        }
        case mWURM:
        {
            break;
        }
        case mMIDBOSS:
        default:
        {
            break;
        }
    }
    if(isEgg)
    {
        eggAI = new changingAI(returnAI, 1, nextSymbol, nextColor);
        returnAI = eggAI;
    }

    return returnAI;
}

unsigned monsterFactory::getPoints(MONSTERTYPE type) const
{
    for(unsigned x = 0; x < _definitions.size(); x++)
    {
        if(_definitions[x].mType == type)
        {
            return _definitions[x].points;
        }
    }
    return 0;
}

