#include "main.h"

#define LINESIZE 49

//Globals


//Enums


//Classes


//Functions



messageQueue::messageQueue() :
     size(0)
{
    //ctor
    for(unsigned x = 0; x < MESSAGEQUEUEY; x++)
    {
        msgQueue[x].text = "";
        msgQueue[x].clr = TCODColor::white;
    }
}

messageQueue::~messageQueue()
{
    //dtor
}

void messageQueue::addMessage(std::string txt, TCODColor clr)
{
    //add a message to the queue, oldest is at 0
    //find out how big the new message is

    std::string tempString = txt;

    do
    {

        if(size >= MESSAGEQUEUEY)
        {
            //ok move everything down to make room
            for(unsigned cX = 1; cX < MESSAGEQUEUEY; cX++)
            {
                msgQueue[cX - 1].text = msgQueue[cX].text;
                msgQueue[cX - 1].clr = msgQueue[cX].clr;
            }

            msgQueue[MESSAGEQUEUEY-1].text = tempString.substr(0, LINESIZE);
            msgQueue[MESSAGEQUEUEY-1].clr = clr;

        }else
        {
            msgQueue[size].text = tempString.substr(0, LINESIZE);
            msgQueue[size].clr = clr;
            size++;
        }

        if(tempString.size() > LINESIZE)
            tempString = tempString.substr(LINESIZE);
        else
            tempString = "";
    }while(tempString.size() > 0);
}

message messageQueue::getMessage(unsigned pos) const
{
    //grab a message at pos
    message tempMessage;
    if(pos <= size)
    {
        return msgQueue[pos];
    }else
    {
        tempMessage.text = "";
        tempMessage.clr = TCODColor::white;
        return tempMessage;
    }
}

void messageQueue::emptyQueue(void)
{
    for(unsigned x = 0; x < MESSAGEQUEUEY; x++)
    {
        addMessage("",TCODColor::white);
    }
    size = 0;
}
