#ifndef AI_H
#define AI_H
class actor;

class ai
{
protected:
    enum aiType{
    PLAYERAI, MONSTERAI
    };
protected:
    virtual bool moveOrAttack(actor* owner, unsigned newX, unsigned newY);
    virtual void muddyCoords(int & playerX, int & playerY, unsigned intensity);
    void getVector(int & xDistance, int & yDistance, float trueDistance);

public:
    ai();
    virtual ~ai();
    virtual void update(actor* owner) = 0;
};

class playerAI : public ai
{
public:
    pickable* _arrow;
private:
    bool moveOrAttack(actor* owner, unsigned newX, unsigned newY);
    //std::vector<std::string> getActorNames(TCODList<Actor *> group);
    //std::vector<std::string> getItemNames(TCODList<Actor *> group); //does an ID check before pushing the names into the vector
public:
    playerAI(pickable* arrow = NULL);
    void update(actor* owner);
};

class explodingCorpseAI : public ai
{
private:
    int _ticksTillExploding;
    int _radius;
    void explode(actor* owner);
public:
    ~explodingCorpseAI();
    explodingCorpseAI(int ticksStart, int radius);
    void update(actor* owner);
};

class changingAI : public ai
{
    ai* _nextAI;
    unsigned _turns;
    unsigned char _nextSymbol;
    TCODColor _nextColor;
public:
    changingAI(ai* nextAI, unsigned turns, unsigned char nextSymbol, TCODColor nextColor);
    ~changingAI();
    void update(actor* owner);
};

class antAI :  public ai
{
    DIRECTIONS _facing;
private:
    bool moveOrAttack(actor* owner, unsigned newX, unsigned newY);
public:
    antAI();
    void update(actor* owner);
};


class bullyAI   :   public ai
{
    float _searchRadius;
private:
    bool moveOrAttack(actor* owner, unsigned newX, unsigned newY);
public:
    bullyAI(float searchRadius);
    void update(actor* owner);
};

class diggerAI:   public ai
{
    float _searchRadius;
private:
    bool moveOrAttack(actor* owner, unsigned newX, unsigned newY);
public:
    diggerAI(float searchRadius);
    void update(actor* owner);
};

/*
class pulserAI    :   public ai
{
public:
    void update(actor* owner);
};

class shooterAI   :   public ai
{
public:
    void update(actor* owner);
};

*/
class tacklerAI    :   public ai
{
    bool _haveMoved;
private:
    bool moveOrAttack(actor* owner, unsigned newX, unsigned newY);
public:
    tacklerAI();
    void update(actor* owner);
};
/*

class wurmAI    :   public ai
{
public:
    void update(actor* owner);
};

class monsterAI: public ai
{
private:
    bool moveOrAttack(Actor* owner, unsigned newX, unsigned newY);
public:
    void update(Actor* owner);
};
*/
#endif
